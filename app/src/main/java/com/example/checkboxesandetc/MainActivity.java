package com.example.checkboxesandetc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void displayToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(View view) {
        StringBuilder mOrderMessage = new StringBuilder("Toppings: ");
        boolean hasChecked = false;

        List<CheckBox> checkBoxList = new ArrayList<>();
        checkBoxList.add((CheckBox) findViewById(R.id.checkBox_chocolate_syrup));
        checkBoxList.add((CheckBox) findViewById(R.id.checkBox_sprinkles));
        checkBoxList.add((CheckBox) findViewById(R.id.checkBox_crushed_nuts));
        checkBoxList.add((CheckBox) findViewById(R.id.checkBox_cherries));
        checkBoxList.add((CheckBox) findViewById(R.id.checkBox_oreo_cookie_crumbles));

        for (CheckBox checkBox : checkBoxList) {
            if (checkBox.isChecked()) {
                mOrderMessage.append(checkBox.getText()).append(" ");
                hasChecked = true;
            }
        }

        if (hasChecked) {
            Toast.makeText(this, mOrderMessage.toString(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "No checkboxes selected.", Toast.LENGTH_SHORT).show();
        }
    }
}